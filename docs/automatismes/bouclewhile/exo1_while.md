---
title: Thème boucle while
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Écrire un programme Python de trois lignes de code au plus, qui affiche le plus petit entier positif $n$ tel $n^{2}-1000n-10000 \geqslant 10^{6}$.
    


{{IDE("exo1/exo1_while")}} 

