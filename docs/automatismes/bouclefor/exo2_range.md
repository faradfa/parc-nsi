---
title: Thème Boucle bornée (for)
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Écrire un programme Python de deux lignes de code au plus,  qui affiche tous les entiers entre 0 et 10 inclus dans l'ordre croissant (un par ligne).
    


{{IDE("exo2/exo2_range")}} 


[Correction](scripts/exo2/corr_exo2_range.py)