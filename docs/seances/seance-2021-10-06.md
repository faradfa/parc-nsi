---
title: Séance du 06/10/2021
---

# Séance du 06/10/2021

## Découverte de la plateforme Upylab 

* Accès : [https://upylab2.ulb.ac.be](https://upylab2.ulb.ac.be)  :  traiter six  exercices du chapitre fonctions  :
    * somme
    * valeur absolue
    * meilleur tarif
    * chiffre
    * nombre de chiffres
    * somme des chiffres

## Chapitre 3 : fonctions, spécification et tests

* [Cours](../chapitre3.md)
* [Activité Capytale ➡️ cours fonctions](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/23a7-84924)


## A faire pour jeudi 7/10 :

[Activité Capytale ➡️ cours fonctions](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/23a7-84924) ➡️ entraînements 2 et 3


## A faire pour la semaine prochaine :

1. Sur [France ioi](http://www.france-ioi.org/) finir le chapitre 5.
2. Réviser les chapitres 1 et 2 (programmation en Python mais pas HTML/CSS) pour l'évaluation sommative n°1 du jeudi 14/10.

## A faire pour le 21/10/2021 :

* Projet HTML/CSS de réalisation d'un mini-site sur un thème en relation avec l'histoire de l'informatique. Chosir un sujet par binôme.
* Le cahier des charges, les [sujets](../sujets-html-css.md) et des exemples de ressources ont été proposés aux élèves.
* [Modèle de site fourni](../Projets/Projets2020/HTML-CSS-Histoire/modele.zip)
