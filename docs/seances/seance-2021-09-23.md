---
title: Séance du 23/09/2021
---

# Séance du 23/09/2021

## Consignes et méthode de travail :

* Matériel :
    * Un classeur avec des feuilles pour écrire et des pochettes transparentes pour ranger les cours
    * Mettre le site <https://parc-nsi.github.io/premiere/> dans les favoris de son navigateur, navigation responsive adaptée aux smartphone : toutes les ressources (cours, corrigés) sont publiés et disponibles sur ce site. Trois rubriques à retenir :
        * La [progression](https://parc-nsi.github.io/premiere/) avec les liens vers les chapitres
        * La liste des [séances](https://parc-nsi.github.io/premiere/seances/) détaillées qui sont ensuite copiées dans Pronote.
        * La rubrique [Automatismes](https://parc-nsi.github.io/premiere/automatismes/) avec des liens vers des QCM externes et des exercices pour travailler les automatismes.
    * Une clef USB de 8 Go minimum, cet [article](https://www.boulanger.com/ref/872118) n'est pas cher.
    * Le manuel Hachette NSI version papier fourni par la région de référence `978-2-01-786630-5`, accessible en ligne sur <https://mesmanuels.fr/acces-libre/3813624>

* Méthode de travail :
    * D'une séance à l'autre : relire le cours, faire les exercices
    * Pendant la séance : alternance de temps d'activités et de synthèse, travail sur des projets    à rendre
    * Évaluations :
        * Rendu de mini-projet ou de projet plus conséquent (pendant les vacances) : travail en classe et à la maison en binôme, évaluations écrites ou orales
        * Formatives sous forme d'interrogations courtes (format QCM) ou d'exposés oral (histoire de l'informatique, synthèse de cours)
        * Sommatives sous forme de devoir d'une heure ou de TP noté


## Chapitre 1: constructions de bases en langage Python

!!! tip "France IOI Concours Algoréa 2015 tour 2"

    Retour sur quelques exercices du chapitre 3 _Répétitions d'instructions_ de [France IOI](http://www.france-ioi.org) :

    * [Feu de forêt A](http://www.france-ioi.org/algo/task.php?idChapter=925&idTask=2909),  une [solution](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/france-ioi/algorea/2015/feu_foretA.py)
    * [Feu de forêt B](http://www.france-ioi.org/algo/task.php?idChapter=644&idTask=1891), une [solution](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/france-ioi/algorea/2015/feu_foretB.py)

!!! info "Présentation du service Capytale"

    * Partage par le professeur de ce [carnet](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/d9ea-62550) avec les corrections de quelques exercices du manuel.


!!! tip "TP2"

    On  finit le [TP2](../chapitre1/TP2/TP2.pdf) de l'exercice 4 jusqu'à l'exercice 9.



## Chapitre 2 : révisions HTML/CSS

!!! info "Le point sur le langage HTML"

    * [Synthèse de cours](../chapitre2/memo/MemoHTML-CSS-2020.pdf)  ➡️ pages 1 à 3
    * Activités proposées dans [le mini-site portable](https://filesender.renater.fr/?s=download&token=27f5ebec-3d2f-4649-82f1-5a1080b71d9f)  :
        * Dans `site/premiere_page_html/html_balises.html` faire les exercices 2, 3, 4 et 5 : lire les consignes sur le mini-site et traiter les exercices dans Capytale :
            * [exercice 2](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/2c4a-61119)
            * [exercice 3](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/0f0c-61116)
            * [exercice 4](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/29bf-61114)
            * [exercice 5](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/6226-61112)
            * [exercice 6](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/29bf-61114)





## A faire pour la semaine prochaine :

1. Sur [France ioi](http://www.france-ioi.org/) traiter la moitié du chapitre 4 du niveau 1.
2. Pour mercredi : chaque groupe prépare une présentation orale de 3 minutes par élève (6 pour un binome, 9 pour un trinome) de son mini-projet.
3. Pour jeudi : interrogation écrite de 30 minutes sur le premier chapitre :
    Quelques QCM pour réviser : 
    

    * Affectations et types de base :
        * [Énoncé](https://genumsi.inria.fr/qcm.php?h=e74b6446b2fb9380f06fe87ff3289bf4>)
        * [Corrigé](https://genumsi.inria.fr/qcm-corrige.php?cle=MTg7MTExOzEzNTsxMjE7MTM4MjsxMzgz>)

    * QCM bilan sur les constructions élémentaires (affectations, tests, boucles) :
        * [Énoncé](https://genumsi.inria.fr/qcm.php?h=74f0a61998145ab12b8d2824c13d1e1a)
        * [Corrigé](https://genumsi.inria.fr/qcm-corrige.php?cle=MTg7MTk7MjM7MjQ7MjU7MjY7Mjc7NDQ7NDU7NDY7NTQ7MTIxOzEzNjsxNDA7MTgyOzIxMjszNjY7MzgzOzM4NDszODU=)

    * QCM sur les fonctions (partie 1, sans tableaux) :
        * [Énoncé](https://genumsi.inria.fr/qcm.php?h=b8fc2a0db42e73eb293891e24c349d08)
        * [Corrigé](https://genumsi.inria.fr/qcm-corrige.php?cle=NDE7NDI7NDM7NDg7NTQ7MTM4OzE3ODsyMDU7MjA5OzIxMjsyMTM7MzAwOzM4MjszOTg7NDkzOzQ5NDs0OTU=)


## A faire pour le 21/10/2021 :

* Projet HTML/CSS de réalisation d'un mini-site sur un thème en relation avec l'histoire de l'informatique. Chosir un sujet par binôme.
* Le cahier des charges, les [sujets](../sujets-html-css.md) et des exemples de ressources ont été proposés aux élèves.
* [Modèle de site fourni](../rojets/Projets2020/HTML-CSS-Histoire/modele.zip)
