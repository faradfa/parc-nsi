---
title:  Projets
layout: parc
---

# Projets 2021/2022

## Mini-projet 1 : prise en main de Python

* [Énoncé](Projets/MiniProjets2021/Mini-Projet1/sujets/NSI-MiniProjets1-Sujets.pdf)
* [Corrigé pdf](Projets/MiniProjets2021/Mini-Projet1/correction/Correction_Mini_Projet_1.pdf)
* [Corrigé `.ipynb`](Projets/MiniProjets2021/Mini-Projet1/correction/Correction_Mini_Projet_1.ipynb)
* [Corrigé `.py`](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/Projets/MiniProjets2021/Mini-Projet1/correction/Correction_Mini_Projet_1.py)


## Projet 2 : histoire de l'informatique 

* Projet HTML/CSS de réalisation d'un mini-site sur un thème en relation avec l'histoire de l'informatique. 
* Le cahier des charges, les [sujets](sujets-html-css.md) et des exemples de ressources ont été proposés aux élèves.
* [Modèle de site fourni](Projets/Projets2020/HTML-CSS-Histoire/modele.zip)


## Projet 3  : jeu de Nim

* [Énoncé](Projets/Nim/Projet1-Nim-2021.pdf)
* [Archive avec squelettes de codes](Projets/Nim/squelette_eleve.zip)


# Projets 2020/2021

## Projet 1

* Projet HTML/CSS de réalisation d'un mini-site sur un thème en relation avec l'histoire de l'informatique. 
* Le cahier des charges, les [sujets](sujets-html-css.md) et des exemples de ressources ont été proposés aux élèves.
* [Modèle de site fourni](Projets/Projets2020/HTML-CSS-Histoire/modele.zip)

## Projet 2

* Projet de programmation sur le jeu _Bulls and Cows_ permettant de réinvestir les constructions élémentaires du langage Python :
  * [Inscription](https://cloud-lyon.beta.education.fr/s/CZQ256P9zNbAbN7)
  * [Sujet](Projets/Projets2020/BullsCows/PremiereNSI-DM-BullsCows-2021V1.pdf)
  * [Matériel](Projets/Projets2020/BullsCows/ProjetBullsCows.zip)


## Projet 3

* Projet de génération de labyrinthe parfait avec `turtle` :   
    *  le [sujet](Projets/Projets2020/Labyrinthe/Projet_Laby.pdf)
    *  [un squelette de code](Projets/Projets2020/Labyrinthe/squelette_laby.py)

## Projet 4

* Projet sur le thème de la compression.
    * le [sujet](Projets/Projets2020/Compression/PremiereNSI-DM-Compression-2021V2.pdf)
    * le [matériel](Projets/Projets2020/Compression/materiel_compression_eleve.zip)
    * le [bac à sable](https://mybinder.org/v2/gh/parc-nsi/premiere-nsi/master?filepath=Projets/Projets2020/Compression/sandbox_PIL.ipynb) pour ceux qui ne réussiraient pas à installer PIL
    * [notebook basthon](https://frama.link/ExemplesOperationsBytes2)   avec des exemples de manipulations de `bytes`. 
* Pour aller plus loin, les plus motivés peuvent traiter la deuxième partie qui était proposée dans la version initiale du sujet avec l'implémentation l'algorithme de compression par dictionnaire LZW.  [ancienne version du sujet](Projets/Projets2020/Compression/PremiereNSI-DM-Compression-2021V1.pdf). Les ressources se trouvent dans le dossier `Projets/Projets2020/Compression` du dépôt.

   
## Projet final 


* Découverte des interfaces graphiques avec Tkinter :
    * [énoncé pdf](Projets/Projets2020/ProjetFinal/ExempleMorpion/Mini-Projet-2021V1.pdf)
    * [archive zip](Projets/Projets2020/ProjetFinal/ExempleMorpion.zip) => extraire puis compléter/exécuter le fichier élève dans `spyder`
    * [squelette de code sur repl.it](https://replit.com/@fredericjunier/MorpionEleve)  => faites un `fork`.
    * Un autre petit exemple d'interface graphique avec `Tkinter`, un [chronomètre](https://replit.com/@fredericjunier/Chronometre).   [Fichier source](Projets/Projets2020/ProjetFinal/chronometre.py)
* Cadrage du projet final et liste des sujets :
    * [document de cadrage](Projets/Projets2020/ProjetFinal/Cadrage/NSI_Presentation_Projet2021.pdf)
    * [ressources](Projets/Projets2020/ProjetFinal/Cadrage/ressources.zip)